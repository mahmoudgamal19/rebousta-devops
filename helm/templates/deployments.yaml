apiVersion: apps/v1
kind: Deployment
metadata:
  name: mysql
spec:
  selector:
    matchLabels:
      app: mysql
  template:
    metadata:
      labels:
        app: mysql
    spec:
      containers:
        - name: mysql
          image: "{{ .Values.mysql.image.repository }}:{{ .Values.mysql.image.tag }}"
          env:
            - name: MYSQL_ROOT_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: laravel-secret
                  key: ROOT_PASSWORD
            - name: MYSQL_DATABASE
              value: "{{ .Values.mysql.auth.database }}"
            - name: MYSQL_USER
              value: "{{ .Values.mysql.auth.user }}"
            - name: MYSQL_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: laravel-secret
                  key: DB_PASSWORD
          ports:
            - containerPort: 3306
          volumeMounts:
            - name: mysql-persistent-storage
              mountPath: /var/lib/mysql
            - name: initdb-scripts
              mountPath: /docker-entrypoint-initdb.d
      volumes:
        - name: mysql-persistent-storage
          persistentVolumeClaim:
            claimName: mysql-pvc
        - name: initdb-scripts
          configMap:
            name: mysql-initdb-scripts

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: redis
spec:
  selector:
    matchLabels:
      app: redis
  template:
    metadata:
      labels:
        app: redis
    spec:
      containers:
        - name: redis
          image: "{{ .Values.redis.image.repository }}:{{ .Values.redis.image.tag }}"
          ports:
            - containerPort: 6379
          volumeMounts:
            - name: redis-persistent-storage
              mountPath: /data
      volumes:
        - name: redis-persistent-storage
          persistentVolumeClaim:
            claimName: redis-pvc

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: laravel
spec:
  selector:
    matchLabels:
      app: laravel
  template:
    metadata:
      labels:
        app: laravel
    spec:
      containers:
        - name: laravel
          image: "{{ .Values.laravel.image.repository }}:{{ .Values.laravel.image.tag }}"
          env:
            - name: APP_NAME
              value: Laravel
            - name: APP_ENV
              value: local
            - name: APP_KEY
              valueFrom:
                secretKeyRef:
                  name: laravel-secret
                  key: APP_KEY
            - name: APP_DEBUG
              value: "true"
            - name: APP_URL
              value: http://localhost
            - name: LOG_CHANNEL
              value: stack
            - name: DB_CONNECTION
              value: mysql
            - name: DB_HOST
              value: "{{ .Values.config.mysql.host }}"
            - name: DB_PORT
              value: "3306"
            - name: DB_DATABASE
              value: "{{ .Values.config.mysql.database }}"
            - name: DB_USERNAME
              value: "{{ .Values.config.mysql.user }}"
            - name: DB_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: laravel-secret
                  key: DB_PASSWORD
            - name: BROADCAST_DRIVER
              value: log
            - name: CACHE_DRIVER
              value: file
            - name: QUEUE_CONNECTION
              value: sync
            - name: SESSION_DRIVER
              value: file
            - name: SESSION_LIFETIME
              value: "120"
            - name: REDIS_HOST
              value: "{{ .Values.config.redis.host }}"
            - name: REDIS_PASSWORD
              value: null
            - name: REDIS_PORT
              value: "6379"
          ports:
            - containerPort: 8000
          volumeMounts:
            - name: laravel-persistent-storage
              mountPath: /var/www/html
      volumes:
        - name: laravel-persistent-storage
          persistentVolumeClaim:
            claimName: laravel-pvc
