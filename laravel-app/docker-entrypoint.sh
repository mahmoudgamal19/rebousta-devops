#!/bin/bash

# Wait for MySQL to be ready
echo "Waiting for MySQL..."
until mysql -h"${DB_HOST}" -u"${DB_USERNAME}" -p"${DB_PASSWORD}" "${DB_DATABASE}" &>/dev/null; do
  sleep 3
  echo "Waiting for MySQL..."
done

# Run migrations
php artisan migrate --force

# Start the Laravel server
exec php artisan serve --host=0.0.0.0 --port=8000
