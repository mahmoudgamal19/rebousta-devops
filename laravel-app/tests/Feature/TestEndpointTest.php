<?php

namespace Tests\Feature;

use Tests\TestCase;

class TestEndpointTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/test');

        $response->assertStatus(200);
        $response->assertSee('Laravel app is working!');
    }
}
