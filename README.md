# MyApp Deployment

## Prerequisites

- Docker
- Kubernetes
- Helm
- GitLab CI/CD

## Deployment Steps

1. **Build Docker Images:**
   ```sh
   docker build -t myapp/laravel:latest laravel-app/.
   docker build -t myapp/mysql:latest docker/mysql/.
   docker build -t myapp/redis:latest docker/redis/.
   ```

#
